enum cadinalDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = cadinalDirections.East;

console.log(currentDirection);